
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author iUser
 */
public class xotoey {
     static char[][] table = {
        {'-', '-','-'},
        {'-', '-','-'},
        {'-', '-','-'},
    };
    static char winner = '-';
    static boolean isFinish = false;
    static int row, col;
    static char player ='x';
    static int count = 0;
    static Scanner kb = new Scanner(System.in);
    //static void showWelcome() {
    
//}
    static void showTable() {
        System.out.println(" 123");
        for (int row = 0; row < table.length; row++){
            System.out.print(row+1);
            for(int column = 0; column < table[row].length; column++){
                System.out.print(table[row][column]);
            }
            System.out.println("");
        }
    
}
    static void showTurn() {
        System.out.println(player+ "turn");
    
}
    static void input() {
        while (true){
            System.out.println("Please input Row Col :");
            row = kb.nextInt() -1;
            col = kb.nextInt() -1;
            if(table[row][col] == '-'){
                table[row][col] = player;
                count++;
                break;
            }
            System.out.println("Error: at row and col is not empty!");
            
        }
    
}
    static void checkCol() {
        for (int row = 0; row < 3; row++) {
            if (table[row][col] != player) {
                return;
            }
        }
        isFinish = true;
        winner = player;
    }

    static void checkrow() {
        for (int col = 0; col < 3; col++) {
            if (table[row][col] != player) {
                return;
            }
        }
        isFinish = true;
        winner = player;
    }

    static void checkX() {
        if (table[0][0] == player) {
            if (table[1][1] == player) {
                if (table[2][2] == player) {
                    isFinish = true;
                    winner = player;
                }
            }
        } else if (table[0][2] == player) {
            if (table[1][1] == player) {
                if (table[2][0] == player) {
                    isFinish = true;
                    winner = player;

                }
            }
        }

    }

    static void checkDraw() {
        if (count == 9 && winner != 'x' && winner != 'o') {
            isFinish = true;

        }
    }

    static void checkWin() {
        checkCol();
        checkrow();
        checkX();
        checkDraw();

    }

    static void switchPlayer() {
        if (player == 'x') {
            player = 'o';
        } else {
            player = 'x';
        }
    }

    static void showResult() {
        showTable();
        if (winner == 'x' || winner == 'o') {
            System.out.println("Player " + winner + " Win...");
        } else {
            System.out.println("Draw...");
        }
    }

    static void showBye() {
        System.out.println("Bye Bye ...");

    }

    public static void main(String[] args) {
        System.out.println("Welcome to OX Game");
        do {
            count++;
            showTable();
            showTurn();
            input();
            checkWin();
            switchPlayer();
        } while (!isFinish);
        showResult();
        showBye();

    }
}
